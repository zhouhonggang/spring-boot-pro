package com.yumoart.museum.commons.enumerate;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 统一返回枚举封装
 */
public enum TypeEnumerate {
    /**
     * 操作类型
     */
    INSERT("insert"),
    UPDATE("update"),
    DELETE("delete"),
    SELECT("select"),
    LOGIN("login"),
    LOGOUT("logout");
    private String value;
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    TypeEnumerate(String value) {
        this.value = value;
    }
}
