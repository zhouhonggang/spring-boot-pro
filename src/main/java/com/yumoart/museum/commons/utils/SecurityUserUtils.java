package com.yumoart.museum.commons.utils;

import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import org.springframework.stereotype.Component;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 获取当前登陆用户信息
 */
@Component
public class SecurityUserUtils {

    /**
     * 获取当前登陆用户主键
     * @return 用户主键
     */
    public Integer getCurrentUserId() {
        return getCurrentUser().getId();
    }

    /**
     * 获取当前登陆人信息
     * @return 用户属性
     */
    public SystemUser getCurrentUser() {
        // 未完待续: 需要绑定SpringSecurity获取信息
        return new SystemUser();
    }

}
