package com.yumoart.museum.commons.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 生成工具类
 */
public class GenerationUtils {

    /**
     * 生成原生UUID
     * @return A-B-C-D
     */
    public static String nativeUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 生成全小写UUID
     * @return
     */
    public static String lowercaseUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成全大写UUID
     * @return
     */
    public static String uppercaseUUID() {
        return lowercaseUUID().toUpperCase();
    }

    /**
     * 生成年月日字符串
     * @return 年月日
     */
    public static String generationDay() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return date.format(formatter);
    }

    /**
     * 生成时分秒字符串
     * @return 时分秒
     */
    public static String generationTime() {
        LocalTime date = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
        return date.format(formatter);
    }

    /**
     * 生成毫秒字符串
     * @return 年月日时分秒毫秒
     */
    public static String generationMillisecond() {
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        return date.format(formatter);
    }

}
