package com.yumoart.museum.commons.constants;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 常量池
 */
public class Constants {
    public final static String LOGIN_SUCCESS = "登陆成功";
    public final static String LOGIN_FAILURE = "登陆失败";
    public final static String USER_NOT_FOUND = "账号不存在";
    public final static String USER_WAS_LOCKED = "账号已锁定";
    public final static String ANONYMOUS_ACCESS = "匿名访问";
    public final static String UNAUTHORIZED_ACCESS = "越权访问";

    public static final String OPERATION_ADD_FAILURE = "添加失败";
    public static final String OPERATION_ADD_SUCCESS = "添加成功";
    public static final String OPERATION_EDIT_FAILURE = "编辑失败";
    public static final String OPERATION_EDIT_SUCCESS = "编辑成功";
    public static final String OPERATION_EDIT_REVISION = "编辑冲突";
    public static final String OPERATION_QUERY_FAILURE = "查询失败";
    public static final String OPERATION_QUERY_SUCCESS = "查询成功";
    public static final String OPERATION_DELETE_FAILURE = "删除失败";
    public static final String OPERATION_DELETE_SUCCESS = "删除成功";
    public static final String OPERATION_DELETE_CHILDREN = "存在子集";

    public final static String DUPLICATE_PHONE_NUMBER = "手机号码重复";
}
