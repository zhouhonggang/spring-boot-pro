package com.yumoart.museum.commons.result;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yumoart.museum.commons.utils.WebServletUtils;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-02-06
 * @description: JSON响应
 */
@Component
public class JSONResponse {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 返回服务器JSON格式响应
     * 1.把result转为json字符串
     * 2.把json字符串响应到客户端
     * @param result code msg data
     */
    public void result(Result result, HttpStatus status) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpServletResponse response = WebServletUtils.getResponse();
        response.setStatus(status.value());
        response.setContentType("application/json;charset=UTF-8");
        try {
            Writer writer = response.getWriter();
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
