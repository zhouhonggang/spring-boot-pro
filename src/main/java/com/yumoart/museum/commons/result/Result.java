package com.yumoart.museum.commons.result;

import com.yumoart.museum.commons.enumerate.ResultEnumerate;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 统一结果集
 */
@Getter
@Setter
public class Result<T> {

    /** 业务状态码 */
    private int code;
    /** 执行结果消息 */
    private String msg;
    /** 返回结果集 */
    private T data;

    public Result() {}

    public Result(Integer code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    /**
     * 成功 | 默认成功
     * @return Result
     */
    public static Result success() {
        Result result = new Result();
        result.setResultEnumerate(ResultEnumerate.INTERFACE_CALL_SUCCESS);
        return result;
    }

    /**
     * 成功 | 默认成功 + 返回数据
     * @return Result
     */
    public static Result success(Object data) {
        Result result = new Result();
        result.setResultEnumerate(ResultEnumerate.INTERFACE_CALL_SUCCESS);
        result.setData(data);
        return result;
    }

    /**
     * 自定义成功 | 指定成功
     * @return Result
     */
    public static Result success(ResultEnumerate resultCode) {
        Result result = new Result();
        result.setResultEnumerate(resultCode);
        return result;
    }

    /**
     * 自定义成功 | 指定成功 + 指定数据
     * @return Result
     */
    public static Result success(ResultEnumerate resultCode, Object data) {
        Result result = new Result();
        result.setResultEnumerate(resultCode);
        result.setData(data);
        return result;
    }

    /**
     * 错误信息 | 默认错误
     * @return Result
     */
    public static Result failure() {
        Result result = new Result();
        result.setResultEnumerate(ResultEnumerate.INTERFACE_INNER_ERROR);
        return result;
    }

    /**
     * 错误信息 | 指定错误
     * @return Result
     */
    public static Result failure(ResultEnumerate resultCode) {
        Result result = new Result();
        result.setResultEnumerate(resultCode);
        return result;
    }

    /**
     * 错误信息 | 默认错误 + 指定数据
     * @return Result
     */
    public static Result failure(Object data) {
        Result result = new Result();
        result.setData(data);
        result.setResultEnumerate(ResultEnumerate.INTERFACE_INNER_ERROR);
        return result;
    }

    /**
     * 错误信息 | 默认错误 + 指定数据
     * @return Result
     */
    public static Result failure(ResultEnumerate resultCode, Object data) {
        Result result = new Result();
        result.setData(data);
        result.setResultEnumerate(resultCode);
        return result;
    }

    public void setResultEnumerate(ResultEnumerate code) {
        this.code = code.getCode();
        this.msg = code.getMessage();
    }

}
