package com.yumoart.museum.commons.annotation;

import com.yumoart.museum.commons.enumerate.TypeEnumerate;

import java.lang.annotation.*;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 日志记录
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoggerOperation {
    /**
     * 被操作的模块: 模块名称
     */
    String module() default "";
    /**
     * 方法描述: 可使用占位符获取参数:{{name}}
     */
    String message() default "";
    /**
     * 操作类型(enum): insert,delete,update,select
     */
    TypeEnumerate type() default TypeEnumerate.SELECT;
}
