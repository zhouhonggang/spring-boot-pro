package com.yumoart.museum.commons.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 基础逻辑层实现
 */
@Transactional(
    readOnly = true,
    rollbackFor = Exception.class
)
public abstract class BaseService<D extends BaseMapper<T>, T extends Base> {

    /**
     * 当前模块数据层实现
     */
    @Autowired
    protected D dao;

    /**
     * 获取当前模块数据层实现
     * @return 数据层实现
     */
    protected D getDao() {
        return dao;
    }

}
