package com.yumoart.museum.projectmodules.educational.controller;

import com.yumoart.museum.projectmodules.educational.entity.EducationalCampus;
import com.yumoart.museum.projectmodules.educational.service.EducationalCampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 教务中心-校区接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-07
 * @description: 教务中心-校区接口
 */
@RestController
@RequestMapping("educational/campus")
public class EducationalCampusController {

    @Autowired
    private EducationalCampusService educationalCampusService;

    /**
     * 查询校区及下属部门岗位
     * @return 集合-子集
     */
    @GetMapping("tree")
    public List<EducationalCampus> tree()
    {
        return educationalCampusService.queryEducationalCampusTree(0);
    }

}
