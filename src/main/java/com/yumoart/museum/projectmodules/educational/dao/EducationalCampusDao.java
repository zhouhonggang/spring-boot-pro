package com.yumoart.museum.projectmodules.educational.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yumoart.museum.projectmodules.educational.entity.EducationalCampus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-07
 * @description: 校区数据层
 */
public interface EducationalCampusDao extends BaseMapper<EducationalCampus> {
    /**
     * 查询校区及下属部门岗位
     * @param parentId 0
     * @return 集合-子集
     */
    List<EducationalCampus> queryEducationalCampusTree(@Param("id") int parentId);
}
