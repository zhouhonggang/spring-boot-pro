package com.yumoart.museum.projectmodules.educational.service;

import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.projectmodules.educational.dao.EducationalCampusDao;
import com.yumoart.museum.projectmodules.educational.entity.EducationalCampus;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-07
 * @description: 校区逻辑层
 */
@Service
public class EducationalCampusService extends BaseService<EducationalCampusDao, EducationalCampus> {

    /**
     * 查询校区及下属部门岗位
     * @param parentId 0
     * @return 集合-子集
     */
    public List<EducationalCampus> queryEducationalCampusTree(@Param("id") int parentId)
    {
        return dao.queryEducationalCampusTree(parentId);
    }

}
