package com.yumoart.museum.projectmodules.educational.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.base.Base;
import com.yumoart.museum.components.validate.group.UpdateValid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-07
 * @description: 校区属性
 */
@Getter
@Setter
@TableName(value = "educational_campus")
public class EducationalCampus extends Base {
    /** 主键ID */
    @TableId(type= IdType.AUTO)
    private Integer id;

    /** 校区/部门名称 */
    private String campusName;

    /** 校区/部门简称 */
    private String campusShorthand;

    /** 上级校区外键 */
    private Integer campusParentId;

    /** 排序 */
    private Integer campusSort;

    /** 校区类型;1.地区2.校区.3部门 */
    private Integer campusCategory;

    /** 启用状态 */
    private Integer campusLocked;

    /** 本校区子集 */
    @TableField(exist = false)
    private List<EducationalCampus> children;

}
