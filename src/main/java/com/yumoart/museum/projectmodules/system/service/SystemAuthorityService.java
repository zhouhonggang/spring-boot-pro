package com.yumoart.museum.projectmodules.system.service;

import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.projectmodules.system.dao.SystemAuthorityDao;
import com.yumoart.museum.projectmodules.system.entity.SystemAuthority;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-09
 * @description: 权限逻辑层
 */
@Service
public class SystemAuthorityService extends BaseService<SystemAuthorityDao, SystemAuthority> {

    /**
     * 查询菜单及子菜单
     * @param parentId 0
     * @return 集合-子集
     */
    public List<SystemAuthority> querySystemAuthorityTree(int parentId)
    {
        return dao.querySystemAuthorityTree(parentId);
    }

}