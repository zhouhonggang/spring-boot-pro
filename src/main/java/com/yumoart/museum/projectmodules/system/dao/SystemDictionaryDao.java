package com.yumoart.museum.projectmodules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionary;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 字典数据层
 */
public interface SystemDictionaryDao extends BaseMapper<SystemDictionary> {
}
