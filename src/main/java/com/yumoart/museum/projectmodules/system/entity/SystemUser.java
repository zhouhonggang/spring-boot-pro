package com.yumoart.museum.projectmodules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.annotation.DictionaryOperation;
import com.yumoart.museum.commons.base.Base;
import com.yumoart.museum.components.validate.calibrator.Password;
import com.yumoart.museum.components.validate.group.UpdateValid;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import java.time.LocalDate;

/**
 * 用户属性
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 用户属性
 */
@Getter
@Setter
@TableName(value = "system_user")
public class SystemUser extends Base {
    /** 主键ID */
    @TableId(type= IdType.AUTO)
    @NotNull(message = "编辑时主键ID为必填项", groups = UpdateValid.class)
    private Integer id;

    @NotBlank(message = "真实姓名不允许为空")
    @Length(min = 2, max = 10, message = "账号长度要求在{min}-{max}之间")
    private String name;

    @NotBlank(message = "昵称不允许为空")
    @Length(min = 2, max = 15, message = "昵称长度要求在{min}-{max}之间")
    private String nick;

    @NotBlank(message = "手机号码不允许为空")
    @Pattern(
        regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$",
        message = "请输入正确的手机号码"
    )
    private String phone;

    @NotBlank(message = "密码不允许为空")
    @Length(min = 6, max = 16, message = "登陆密码长度要求在{min}-{max}之间")
    @Password(message = "登陆密码强度太低风险太高")
    private String pass;

    @NotBlank(message = "用户头像不允许为空")
    private String avatar;

    @NotNull(message = "性别不允许为空")
    @Range(min = 1, max = 2, message = "请选择正确性别")
    @DictionaryOperation(code = "gender")
    private Integer gender;

    @NotNull(message = "用户锁定状态不允许为空")
    private Integer locked = 0;

    @NotBlank(message = "电子邮箱不允许为空")
    @Email(message = "电子邮箱必须符合规范")
    @Length(min = 6, max = 30, message = "电子邮箱长度要求在{min}-{max}之间")
    private String email;

    @NotNull(message = "出生日期不允许为空")
    @Past(message = "出生日期必须是当前日期之前")
    // @JsonSerialize(using = DefaultSerializer.class)
    private LocalDate birthday;

    @NotBlank(message = "家庭住址不允许为空")
    @Length(min = 2, max = 60, message = "家庭住址长度要求在{min}-{max}之间")
    private String address;

    @Length(min = 2, max = 900, message = "个人介绍长度要求在{min}-{max}之间")
    private String remarks;
}
