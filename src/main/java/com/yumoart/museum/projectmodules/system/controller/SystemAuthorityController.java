package com.yumoart.museum.projectmodules.system.controller;

import com.yumoart.museum.projectmodules.system.entity.SystemAuthority;
import com.yumoart.museum.projectmodules.system.service.SystemAuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统管理-菜单接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-09
 * @description: 系统-权限接口
 */
@RestController
@RequestMapping("system/authority")
public class SystemAuthorityController {

    private SystemAuthorityService systemAuthorityService;

    @Autowired
    public SystemAuthorityController(SystemAuthorityService systemAuthorityService) {
        this.systemAuthorityService = systemAuthorityService;
    }

    /**
     * 查询菜单及子菜单
     * @return 集合-子集
     */
    @GetMapping("tree")
    public List<SystemAuthority> tree()
    {
        return systemAuthorityService.querySystemAuthorityTree(0);
    }

}
