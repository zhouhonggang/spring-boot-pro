package com.yumoart.museum.projectmodules.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.projectmodules.system.dao.SystemDictionaryDetailDao;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionaryDetail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 字典详情逻辑层
 */
@Service
public class SystemDictionaryDetailService extends BaseService<SystemDictionaryDetailDao, SystemDictionaryDetail> {

    /**
     * 根据数据字典主键查询详情表
     * @param id 数据字典主键
     * @return 数据字典详情集合
     */
    public List<SystemDictionaryDetail> loadDetailById(int id)
    {
        QueryWrapper<SystemDictionaryDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("dictionary_id", id);
        return dao.selectList(wrapper);
    }

    /**
     * 根据数据字典编码查询详情表
     * @param code 数据字典编码
     * @return 数据字典详情集合
     */
    public List<Map<String, Object>> loadDetailByCode(String code)
    {
        return dao.loadDetailByCode(code);
    }

    /**
     * 根据数据字典编码与值查询名称
     * @param code 数据字典编码
     * @param value 数据字典详情值
     * @return 数据字典详情表现值
     */
    public String loadDetailNameByCode(String code, int value) {
        return dao.loadDetailNameByCode(code, value);
    }

    /**
     * 添加
     * @param entity 属性
     * @return 条数
     */
    @Transactional
    public int save(SystemDictionaryDetail entity)
    {
        return dao.insert(entity);
    }

    /**
     * 根据主键ID删除
     * @param id 主键ID
     * @return 条数
     */
    @Transactional
    public int delete(int id)
    {
        return dao.deleteById(id);
    }

}
