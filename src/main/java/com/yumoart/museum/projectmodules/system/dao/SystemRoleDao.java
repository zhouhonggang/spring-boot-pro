/*
* Copyright (c) [2022] [zhouhonggang]
*
* [spring-boot-pro] is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
*  http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*/
package com.yumoart.museum.projectmodules.system.dao;

import com.yumoart.museum.projectmodules.system.entity.SystemRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zhouhonggang
* @version 1.0.0
* @project spring-boot-pro
* @datetime 2023-01-30
* @description: [系统管理_角色管理·数据]
*/
public interface SystemRoleDao extends BaseMapper<SystemRole> {

}
