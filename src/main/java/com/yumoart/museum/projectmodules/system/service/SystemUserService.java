package com.yumoart.museum.projectmodules.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.commons.constants.Constants;
import com.yumoart.museum.commons.result.Result;
import com.yumoart.museum.projectmodules.system.dao.SystemUserDao;
import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 用户逻辑层
 */
@Service
@CacheConfig(cacheNames = "system:user")
public class SystemUserService extends BaseService<SystemUserDao, SystemUser> {

    /**
     * 用户注册
     * @param entity 用户属性
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result save(SystemUser entity) {
        // 判断手机号码是否存在
        if(this.queryPhoneExist(entity.getPhone()) > 0) {
            return Result.failure(Constants.DUPLICATE_PHONE_NUMBER);
        }
        // 判断执行结果是否正确
        if(dao.insert(entity) == 0) {
            return Result.failure(Constants.OPERATION_ADD_FAILURE);
        }
        // 返回执行成功
        return Result.success(Constants.OPERATION_ADD_SUCCESS);
    }

    /**
     * 用户编辑
     * @param entity 用户属性
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result update(SystemUser entity) {
        // 判断执行结果是否正确
        if(dao.updateById(entity) == 0) {
            return Result.failure(Constants.OPERATION_EDIT_FAILURE);
        }
        // 返回执行成功
        return Result.success(Constants.OPERATION_EDIT_SUCCESS);
    }

    /**
     * 根据主键ID查询用户属性
     * @param id 主键ID
     * @return 用户属性
     */
    @Cacheable
    public SystemUser load(int id) {
        return dao.selectById(id);
    }

    /**
     * 分页条件查询
     * @param page  页码
     * @param size  条数
     * @param entity    条件
     * @return  分页对象
     */
    @Cacheable(key = "'page'+#page+':'+#size+':'+#entity")
    public Page<SystemUser> page(int page, int size, SystemUser entity) {
        return dao.selectPage(Page.of(page, size), new QueryWrapper<SystemUser>());
    }

    /**
     * 查询手机号是否存在
     * @param phone 手机号
     * @return 结果集
     */
    public Long queryPhoneExist(String phone) {
        return dao.selectCount(
            new QueryWrapper<SystemUser>()
                .lambda()
                .eq(SystemUser::getPhone, phone)
        );
    }

    /**
     * 查询性别为男或女用户 => SELECT gender FROM system_user WHERE (gender = ?)
     * @param gender 性别
     * @return 用户集合
     */
    public List<SystemUser> queryByCondition(int gender) {
        return dao.selectList(
            new QueryWrapper<SystemUser>()
                .lambda()
                .eq(SystemUser::getGender, gender)
        );
    }

}
