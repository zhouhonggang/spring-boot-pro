/*
* Copyright (c) [2022] [zhouhonggang]
*
* [spring-boot-pro] is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
*  http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*/
package com.yumoart.museum.projectmodules.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.yumoart.museum.commons.annotation.LoggerOperation;
import com.yumoart.museum.commons.enumerate.TypeEnumerate;
import com.yumoart.museum.commons.result.Result;
import com.yumoart.museum.components.validate.group.UpdateValid;
import com.yumoart.museum.projectmodules.system.entity.SystemRole;
import com.yumoart.museum.projectmodules.system.service.SystemRoleService;

import jakarta.validation.groups.Default;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *[系统管理_角色管理·API接口]
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-30
 * @description: [系统管理_角色管理·接口]
 */
@RestController
@RequestMapping("system/role")
public class SystemRoleController {

    @Autowired
    private SystemRoleService systemRoleService;

    /**
     * 添加系统管理_角色管理
     * @param entity 系统管理_角色管理属性
     * @return 结果集
     */
    @PostMapping
    @LoggerOperation(module = "系统管理_角色管理", message = "添加系统管理_角色管理", type = TypeEnumerate.INSERT)
    public Result insert(@RequestBody @Validated SystemRole entity) {
        return systemRoleService.insert(entity);
    }

    /**
     * 编辑系统管理_角色管理
     * @param entity 系统管理_角色管理属性
     * @return 结果集
     */
    @PutMapping
    @LoggerOperation(module = "系统管理_角色管理", message = "编辑系统管理_角色管理", type = TypeEnumerate.UPDATE)
    public Result update(@RequestBody @Validated({UpdateValid.class, Default.class}) SystemRole entity) {
        return systemRoleService.update(entity);
    }

    /**
     * 删除用户
     * @param id 主键id
     * @return 结果集
     */
    @DeleteMapping("{id}")
    @LoggerOperation(module = "系统管理_角色管理", message = "单删系统管理_角色管理", type = TypeEnumerate.DELETE)
    public Result delete(@PathVariable int id) {
        return systemRoleService.delete(id);
    }

    /**
     * 批量删除系统管理_角色管理
     * @param ids 主键ids
     * @return 结果集
     */
    @DeleteMapping
    @LoggerOperation(module = "系统管理_角色管理", message = "批删系统管理_角色管理", type = TypeEnumerate.DELETE)
    public Result deletes(@RequestBody List<Integer> ids) {
        return systemRoleService.deletes(ids);
    }

    /**
     * 主键ID查询
     * @param id 系统管理_角色管理主键ID
     * @return 系统管理_角色管理属性
     */
    @GetMapping("{id}")
    @LoggerOperation(module = "系统管理_角色管理", message = "主键id查询")
    public SystemRole load(@PathVariable int id) {
        return systemRoleService.queryById(id);
    }

    /**
     * 条件分页查询
     * @param entity 分页条件
     * @return 分页属性
     */
    @GetMapping("page/{page}/{size}")
    @LoggerOperation(module = "系统管理_角色管理", message = "条件分页查询")
    public Page<SystemRole> page(
        @PathVariable int page,
        @PathVariable int size,
        @RequestBody SystemRole entity) {
        return systemRoleService.page(page, size, entity);
    }

}