package com.yumoart.museum.projectmodules.system.controller;

import com.yumoart.museum.projectmodules.system.service.SystemAccessoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 系统管理-附件接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-20
 * @description: 系统-附件接口
 */
@RestController
@RequestMapping("system/accessory")
public class SystemAccessoryController {

    private SystemAccessoryService systemAccessoryService;

    public SystemAccessoryController(SystemAccessoryService systemAccessoryService) {
        this.systemAccessoryService = systemAccessoryService;
    }

    /**
     * 附件上传
     * @param files 附件组
     * @return 附件名称集合
     */
    @PostMapping
    public List<String> accessory(@RequestParam MultipartFile[] files) {
        return systemAccessoryService.upload(files);
    }

}
