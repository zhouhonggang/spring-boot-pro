package com.yumoart.museum.projectmodules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionaryDetail;

import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 字典详情数据层
 */
public interface SystemDictionaryDetailDao extends BaseMapper<SystemDictionaryDetail> {
    String loadDetailNameByCode(String code, int value);
    List<Map<String, Object>> loadDetailByCode(String code);
}
