package com.yumoart.museum.projectmodules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yumoart.museum.projectmodules.system.entity.SystemAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-09
 * @description: 权限数据层
 */
public interface SystemAuthorityDao extends BaseMapper<SystemAuthority> {

    /**
     * 查询菜单及子菜单
     * @param parentId 0
     * @return 集合-子集
     */
    List<SystemAuthority> querySystemAuthorityTree(@Param("id") int parentId);
}
