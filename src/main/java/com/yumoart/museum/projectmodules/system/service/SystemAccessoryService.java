package com.yumoart.museum.projectmodules.system.service;

import com.yumoart.museum.commons.utils.GenerationUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-20
 * @description: 附件逻辑层实现
 */
@Service
public class SystemAccessoryService {

    @Value("${image.path}")
    private String filePath;

    /**
     * 文件上传
     * @param files 文件集合
     * @return 文件名称集合
     */
    public List<String> upload(MultipartFile[] files) {
        List<String> fileNameList = new ArrayList<>();
        if(!ObjectUtils.isEmpty(files)) {
            for (MultipartFile file : files) {
                // 获取用户上传文件名称
                String fileName = file.getOriginalFilename();
                // 根据名称获取文件后缀
                String fileSuffix = fileName.substring(fileName.lastIndexOf("."));
                // 生成当前唯一文件名称
                String randomName = GenerationUtils.generationMillisecond() + fileSuffix;
                try {
                    //保存附件到指定路径中
                    file.transferTo(Path.of(filePath+randomName));
                    // 添加当前文件名到集合
                    fileNameList.add(randomName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return fileNameList;
    }

}
