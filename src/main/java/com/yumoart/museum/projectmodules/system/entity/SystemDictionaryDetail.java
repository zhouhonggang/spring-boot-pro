package com.yumoart.museum.projectmodules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.base.Base;
import com.yumoart.museum.components.validate.group.UpdateValid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 字典详情属性
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 字典详情属性
 */
@Getter
@Setter
@TableName("system_data_dictionary_detail")
public class SystemDictionaryDetail extends Base {
    /** 主键ID */
    @TableId(type= IdType.AUTO)
    @NotNull(message = "编辑时主键ID为必填项", groups = UpdateValid.class)
    private Integer id;

    /** 字典名称 */
    @NotBlank(message = "字典名称不允许为空")
    @Length(min = 1, max = 60, message = "字典名称长度要求在{min}-{max}之间")
    private String name;

    /** 字典数值 */
    @NotNull(message = "字典数值不允许为空")
    private Integer code;

    /** 字典排序 */
    @NotNull(message = "字典排序不允许为空")
    private Integer sorted;

    /** 字典备注 */
    @NotBlank(message = "字典备注不允许为空")
    private String reason;

    /** 字典外键 */
    @TableField(value = "dictionary_id")
    @NotNull(message = "字典外键不允许为空")
    private Integer dictionaryId;
}
