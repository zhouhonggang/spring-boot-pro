package com.yumoart.museum.projectmodules.system.service;

import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.commons.utils.WebServletUtils;
import com.yumoart.museum.projectmodules.system.dao.SystemLoggerDao;
import com.yumoart.museum.projectmodules.system.entity.SystemLogger;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 日志逻辑层
 */
@Service
public class SystemLoggerService extends BaseService<SystemLoggerDao, SystemLogger> {

    /**
     * 日志记录
     * @param times     耗时
     * @param module    模块
     * @param type      类型
     * @param message   消息
     */
    @Transactional
    public void insert(long times, String module, String type, String message, String params, String result)
    {
        // 获取用户请求
        HttpServletRequest request = WebServletUtils.getRequest();

        SystemLogger entity = new SystemLogger();
        entity.setModule(module);
        entity.setType(type);
        entity.setMessage(message);
        entity.setExecuteParams(params);
        entity.setReturnValue(result);
        entity.setRequestMethod(request.getMethod());
        entity.setRequestIpaddr(request.getRemoteAddr());
        entity.setRequestUrl(request.getRequestURL().toString());
        entity.setExecuteTime(times);
        dao.insert(entity);
    }

}
