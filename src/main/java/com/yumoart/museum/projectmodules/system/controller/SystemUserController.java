package com.yumoart.museum.projectmodules.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yumoart.museum.commons.annotation.LoggerOperation;
import com.yumoart.museum.commons.enumerate.TypeEnumerate;
import com.yumoart.museum.commons.result.Result;
import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import com.yumoart.museum.projectmodules.system.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统管理-用户接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 系统-用户接口
 */
@RestController
@RequestMapping("system/user")
public class SystemUserController {

    private SystemUserService systemUserService;

    @Autowired
    public SystemUserController(SystemUserService systemUserService) {
        this.systemUserService = systemUserService;
    }

    /**
     * 注册用户
     * @param entity 用户属性
     */
    @PostMapping
    @LoggerOperation(module = "系统-用户", message = "用户注册", type = TypeEnumerate.INSERT)
    public Result save(@RequestBody SystemUser entity) {
        return systemUserService.save(entity);
    }

    /**
     * 编辑用户
     * @param entity 用户属性
     */
    @PutMapping
    @LoggerOperation(module = "系统-用户", message = "信息编辑", type = TypeEnumerate.UPDATE)
    public Result update(@RequestBody SystemUser entity) {
        return systemUserService.update(entity);
    }

    /**
     * 根据主键ID查询用户属性
     * @param id 主键ID
     * @return 用户属性
     */
    @GetMapping("{id}")
    @LoggerOperation(module = "系统-用户", message = "主键查询", type = TypeEnumerate.SELECT)
    public SystemUser load(@PathVariable int id ) {
        return systemUserService.load(id);
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 条数
     * @param entity 条件
     * @return 分页对象
     */
    @PostMapping("page/{page}/{size}")
    @LoggerOperation(module = "系统-用户", message = "分页查询", type = TypeEnumerate.SELECT)
    public Page<SystemUser> page(
            @PathVariable int page,
            @PathVariable int size,
            @RequestBody SystemUser entity)
    {
        return systemUserService.page(page, size, entity);
    }

}
