package com.yumoart.museum.projectmodules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.base.Base;
import lombok.Getter;
import lombok.Setter;

/**
 * 日志属性
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 日志属性
 */
@Getter
@Setter
@TableName("system_logger")
public class SystemLogger extends Base {
    /** 主键ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 操作模块 */
    private String module;
    /** 操作方法 */
    private String type;
    /** 操作日志 */
    private String message;
    /** 请求路径 */
    private String requestUrl;
    /** 请求方法 */
    private String requestMethod;
    /** 请求IP地址 */
    private String requestIpaddr;
    /** 耗时时间 */
    private Long executeTime;
    /** 输入参数 */
    private String executeParams;
    /** 返回结果 */
    private String returnValue;
}
