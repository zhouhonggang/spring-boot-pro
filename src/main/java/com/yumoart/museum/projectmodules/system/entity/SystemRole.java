/*
* Copyright (c) [2022] [zhouhonggang]
*
* [spring-boot-pro] is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
*  http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*/
package com.yumoart.museum.projectmodules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.base.Base;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

/**
* @author zhouhonggang
* @version 1.0.0
* @project spring-boot-pro
* @datetime 2023-01-30
* @description: [系统管理_角色管理·属性]
*/
@Getter
@Setter
@TableName(value = "system_role")
public class SystemRole extends Base {
    private static final long serialVersionUID = 1L;
    /** 主键 */
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "主键不可为空")
    private Integer id;
    /** 角色名称 */
    @NotBlank(message = "角色名称不可为空")
    private String name;
    /** 角色编号 */
    @NotBlank(message = "角色编号不可为空")
    private String code;
}