package com.yumoart.museum.projectmodules.system.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yumoart.museum.commons.base.BaseService;
import com.yumoart.museum.projectmodules.system.dao.SystemDictionaryDao;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 字典逻辑层
 */
@Service
public class SystemDictionaryService extends BaseService<SystemDictionaryDao, SystemDictionary> {

    /**
     * 按条件分页查询
     * @param entity 字典信息
     * @return 分页对象
     */
    public Page<SystemDictionary> page(int page, int size, SystemDictionary entity)
    {
        QueryWrapper<SystemDictionary> wrapper = new QueryWrapper<>();
        if(StringUtils.hasLength(entity.getName()))
        {
            wrapper.like("name", entity.getName());
        }
        if(StringUtils.hasLength(entity.getCode()))
        {
            wrapper.like("code", entity.getCode());
        }
        return dao.selectPage(Page.of(page, size), wrapper);
    }

    /**
     * 添加
     * @param entity 属性
     * @return 条数
     */
    @Transactional
    public int save(SystemDictionary entity)
    {
        return dao.insert(entity);
    }

    /**
     * 根据主键ID删除
     * @param id 主键ID
     * @return 条数
     */
    @Transactional
    public int delete(int id)
    {
        return dao.deleteById(id);
    }

}
