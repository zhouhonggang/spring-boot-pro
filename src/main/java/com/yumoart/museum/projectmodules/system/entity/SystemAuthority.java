package com.yumoart.museum.projectmodules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yumoart.museum.commons.base.Base;
import com.yumoart.museum.components.validate.group.UpdateValid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 菜单属性
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-09
 * @description: 权限菜单属性
 */
@Getter
@Setter
@TableName(value = "system_authority")
public class SystemAuthority extends Base {
    /**
     * 主键ID
     * @mock 1
     * @since 1.0.0
     */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 权限名称 */
    private String name;
    /** 权限编码 */
    private String code;
    /** 访问路径 */
    private String url;
    /** 前端名称;前端显示名称 */
    private String frontName;
    /** 前端路径;前端访问路径 */
    private String frontPath;
    /** 前端组件;前端组件路径 */
    private String frontComponent;
    /** 前端图标;前端菜单图标 */
    private String frontIcon;
    /** 前端跳转;重定向跳转地址 */
    private String frontRedirect;
    /** 菜单类型;0:一级 1:二级 2: 按钮 */
    private int frontType;
    /** 菜单排序 */
    private int frontSort;
    /** 打开位置;0:内部打开 1:外部打开 */
    private int frontLocation;
    /** 权限菜单子集 */
    @TableField(exist = false)
    private List<SystemAuthority> children;
}
