package com.yumoart.museum.projectmodules.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yumoart.museum.projectmodules.system.entity.SystemUser;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 用户数据层
 */
public interface SystemUserDao extends BaseMapper<SystemUser> {
}
