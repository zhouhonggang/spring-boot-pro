package com.yumoart.museum.projectmodules.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionary;
import com.yumoart.museum.projectmodules.system.entity.SystemDictionaryDetail;
import com.yumoart.museum.projectmodules.system.service.SystemDictionaryDetailService;
import com.yumoart.museum.projectmodules.system.service.SystemDictionaryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统管理-字典接口
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 数据字典接口
 */
@RestController
@RequestMapping("system/dictionary")
public class SystemDictionaryController {

    @Autowired
    private SystemDictionaryService dictionaryService;
    @Autowired
    private SystemDictionaryDetailService dictionaryDetailService;

    /**
     * 分页查询
     * @param page 起始页码
     * @param size 每页条数
     * @param entity 字典属性
     * @return 分页对象
     */
    @PostMapping("page/{page}/{size}")
    public Page<SystemDictionary> page(
        @PathVariable int page,
        @PathVariable int size,
        @RequestBody SystemDictionary entity)
    {
        return dictionaryService.page(page, size, entity);
    }

    /**
     * 添加字典主表
     * @param entity 字典属性
     */
    @PostMapping
    public void save(@RequestBody @Valid SystemDictionary entity)
    {
        dictionaryService.save(entity);
    }

    /**
     * 添加字典子表
     * @param entity 字典详情属性
     */
    @PostMapping("detail")
    public void save(@RequestBody @Valid SystemDictionaryDetail entity)
    {
        dictionaryDetailService.save(entity);
    }

    /**
     * 根据字典主键获取字典详情
     * @param id 字典主键
     * @return 字典详情属性集合
     */
    @GetMapping("detail/{id}")
    public List<SystemDictionaryDetail> list(@PathVariable int id)
    {
        return dictionaryDetailService.loadDetailById(id);
    }

    /**
     * 根据字典编码获取集合
     * @param code 编码
     * @return 详情集合
     */
    @GetMapping("detail/code/{code}")
    public List<Map<String, Object>> list(@PathVariable String code)
    {
        return dictionaryDetailService.loadDetailByCode(code);
    }

    /**
     * 根据字典主键删除
     * @param id 字典主键
     */
    @DeleteMapping("{id}")
    public void delete(@PathVariable int id)
    {
        dictionaryService.delete(id);
    }

    /**
     * 根据字典详情主键删除
     * @param id 字典详情主键
     */
    @DeleteMapping("detail/{id}")
    public void deleteDetail(@PathVariable int id)
    {
        dictionaryDetailService.delete(id);
    }

}
