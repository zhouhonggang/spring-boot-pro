package com.yumoart.museum.configurations.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yumoart.museum.commons.constants.Constants;
import com.yumoart.museum.commons.enumerate.ResultEnumerate;
import com.yumoart.museum.commons.result.JSONResponse;
import com.yumoart.museum.commons.result.Result;
import com.yumoart.museum.commons.utils.GenerationUtils;
import com.yumoart.museum.components.redis.RedisComponent;
import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import com.yumoart.museum.projectmodules.system.service.SystemUserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-02-04
 * @description: SpringSecurity安全配置
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SpringSecurityConfiguration {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JSONResponse jsonResponse;
    @Autowired
    private RedisComponent redisComponent;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SystemUserService systemUserService;
    /**
     * 重写 SpringSecurity UserDetailsService
     * @return UserDetailsServiceImpl
     */
    public UserDetailsService customUserDetailsService() {
        // 根据登陆账号查询用户对象
        return (phone -> {
            boolean accountNonLocked = true;
            SystemUser systemUser = systemUserService.loadUserByUsername(phone);
            // 提示客户端账号不存在
            if(ObjectUtils.isEmpty(systemUser)) {
                throw new UsernameNotFoundException(Constants.USER_NOT_FOUND);
            // 提示客户端账号已锁定
            } else if(systemUser.getLocked() == 1) {
                // throw new LockedException("很遗憾, 账号已锁定!");
                accountNonLocked = false;
            }
            // 创建空的权限集合列表
            Collection<? extends GrantedAuthority> authorities = new ArrayList<>();
            // 返回Spring Security User对象等待认证结果
            // return new User(phone, systemUser.getPass(), authorities);
            return new User(phone, systemUser.getPass(), true, true, true, accountNonLocked, authorities);
        });
    }

    /**
     * 认证前置检查
     * @return UserDetailsChecker
     */
    public UserDetailsChecker userDetailsChecker()
    {
        return details -> {
            if(!details.isAccountNonLocked()) {
                throw new LockedException(Constants.USER_WAS_LOCKED);
            }
        };
    }

    /**
     * DaoAuthenticationProvider身份认证
     * @return DaoAuthenticationProvider
     * @throws Exception Exception
     */
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() throws Exception {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        //显示用户不存在异常
        provider.setHideUserNotFoundExceptions(false);
        //注入密码解析器
        provider.setPasswordEncoder(passwordEncoder);
        //注入查找用户
        provider.setUserDetailsService(customUserDetailsService());
        //完成前置校验
        provider.setPreAuthenticationChecks(userDetailsChecker());
        provider.afterPropertiesSet();
        return provider;
    }

    /**
     * 实例化SpringSecurity认证管理器
     * @param configuration 认证配置
     * @return AuthenticationManager
     * @throws Exception Exception
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    /**
     * SpringSecurity过滤器链
     * @return SecurityFilterChain
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity, AuthenticationManager authenticationManager) throws Exception {
        httpSecurity
                // 禁用CSRF(csrf跨站点请求攻击 cookie)
                .csrf().disable()
                // 启用CORS(启用跨域访问处理)
                .cors()
            .and()
                // 服务器端会话管理
                .sessionManagement()
                    /**
                     * 会话管理策略 request.getSession(boolean);
                     * 1.SessionCreationPolicy.ALWAYS true
                     * 2.SessionCreationPolicy.NEVER false
                     * 3.SessionCreationPolicy.IF_REQUIRED conditional true
                     * 4.SessionCreationPolicy.STATELESS none
                     */
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                // 关于请求认证规则
                .authorizeHttpRequests()
                    // 定义放行规则
                    .requestMatchers("/system/user/**").permitAll()
                    // 其他一律认证
                    .anyRequest().authenticated()

            .and()
                // 异常处理
                .exceptionHandling()
                    // 403 请求未认证
                    .authenticationEntryPoint((request, response, exception) -> {
                        ResultEnumerate enumerate = ResultEnumerate.LOGIN_NOT_LOGGED;
                        jsonResponse.result(Result.failure(enumerate), HttpStatus.FORBIDDEN);
                    })
                    // 401 请求未授权
                    .accessDeniedHandler((request, response, exception) -> {
                        ResultEnumerate enumerate = ResultEnumerate.UNAUTHORIZED_ACCESS;
                        jsonResponse.result(Result.failure(enumerate), HttpStatus.FORBIDDEN);
                    })
            .and()
                .authenticationProvider(daoAuthenticationProvider())
                // 重写参数收集登录过滤器
                // 拦截 以POST形式访问/login 的请求
                .addFilter(new UsernamePasswordAuthenticationFilter() {

                    /**
                     * 收集前后端分离JSON格式参数
                     * @param request 请求
                     * @param response 响应
                     * @return 认证对象
                     * @throws AuthenticationException
                     */
                    @Override
                    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
                        try {
                            // 前端登录提交的参数
                            Map<String, String> parameters = objectMapper.readValue(request.getInputStream(), Map.class);
                            // SpringSecurity认证对象封装
                            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(parameters.get("username"), parameters.get("password"));
                            // 认证对象交接工作 -> 认证管理器
                            return authenticationManager.authenticate(token);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return super.attemptAuthentication(request, response);
                    }

                    /**
                     * 认证失败
                     * @param request   请求
                     * @param response  响应
                     * @param failed    异常
                     * @throws IOException
                     * @throws ServletException
                     */
                    @Override
                    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
                        // 清空认证对象
                        SecurityContextHolder.clearContext();
                        // 默认其他登陆失败
                        ResultEnumerate enumerate = ResultEnumerate.LOGIN_OTHER_ERROR;
                        // 账号不存在异常
                        if(failed instanceof UsernameNotFoundException) {
                            enumerate = ResultEnumerate.LOGIN_USER_PASS_ERROR;
                        // 密码不正确异常
                        } else if(failed instanceof BadCredentialsException) {
                            enumerate = ResultEnumerate.LOGIN_USER_PASS_ERROR;
                        // 账号被锁定异常
                        } else if(failed instanceof LockedException) {
                            enumerate = ResultEnumerate.LOGIN_USER_LOCKED;
                        }
                        // 返回响应到客户端
                        jsonResponse.result(Result.failure(enumerate), HttpStatus.FORBIDDEN);
                    }

                    /**
                     * 认证成功
                     * @param request   请求
                     * @param response  响应
                     * @param chain     过滤器链
                     * @param authentication 认证对象
                     * @throws IOException
                     * @throws ServletException
                     */
                    @Override
                    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
                        //1.token == cookie
                        String token = GenerationUtils.uppercaseUUID();
                        //2.redis == session
                        //2.1获取认证成功的用户对象
                        User user = (User) authentication.getPrincipal();
                        //2.2获取认证对象中登录账号
                        String username = user.getUsername();
                        //2.3根据登录账号查询用户对象
                        SystemUser systemUser = systemUserService.loadUserByUsername(username);

                        //封装用户完整信息存储到redis中
                        Map<String, Object> redisObject = Map.of(
                            "name", username,
                            "user", systemUser,
                            "authorities", systemUserService.queryAuthorityByUsername(username)
                        );

                        //2.4向redis中写入会话对象
                        redisComponent.hmset(token, redisObject, 60*30);

                        //3.返回服务器响应信息
                        ResultEnumerate enumerate = ResultEnumerate.LOGIN_SUCCESS;
                        Result result = Result.success(enumerate, token);
                        jsonResponse.result(result, HttpStatus.OK);
                    }

                })
                // 拦截 除去[1.登陆 2.放行]以外 所有请求
                .addFilter(new BasicAuthenticationFilter(authenticationManager) {

                    @Override
                    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
                        //1.从请求中获取token信息
                        String authorization = request.getHeader("Authorization");
                        if(StringUtils.hasText(authorization)) {
                            String token = authorization.substring("Bearer ".length());
                            if(StringUtils.hasText(token)) {
                                //2.验证redis中是否存在当前认证信息
                                if(redisComponent.exists(token)) {
                                    //通过token获取登陆账号
                                    String username = (String)redisComponent.hget(token, "name");
                                    //通过token获取登陆权限
                                    List<String> authoritiesList = (List)redisComponent.hget(token, "authorities");
                                    //创建Spring Security认证对象
                                    Collection<GrantedAuthority> authorities = new ArrayList<>();
                                    authoritiesList.forEach(code -> {
                                        authorities.add(new SimpleGrantedAuthority(code));
                                    });

                                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                            new UsernamePasswordAuthenticationToken(username, null, authorities);
                                    //完成Security认证
                                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

                                    //token续约
                                    redisComponent.expire(token, 60*30);
                                }
                            }
                        }
                        super.doFilterInternal(request, response, chain);
                    }
                });
        return httpSecurity.build();
    }

}
