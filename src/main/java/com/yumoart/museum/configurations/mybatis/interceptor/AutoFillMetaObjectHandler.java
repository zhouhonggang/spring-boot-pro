package com.yumoart.museum.configurations.mybatis.interceptor;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.yumoart.museum.commons.utils.SecurityUserUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 公共字段内容自动填充
 */
@Component
public class AutoFillMetaObjectHandler implements MetaObjectHandler {

    @Autowired
    private SecurityUserUtils securityUserUtils;

    /**
     * 添加自动填充
     * @param metaObject 添加对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Integer userId = securityUserUtils.getCurrentUserId();
        this.fillStrategy(metaObject, "revision", 1);
        this.fillStrategy(metaObject, "deleteFlag", 0);
        this.fillStrategy(metaObject, "createdBy", userId);
        this.fillStrategy(metaObject, "createdTime", LocalDateTime.now());
    }

    /**
     * 修改自动填充
     * @param metaObject 修改对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Integer userId = securityUserUtils.getCurrentUserId();
        this.fillStrategy(metaObject, "updatedBy", userId);
        this.fillStrategy(metaObject, "updatedTime", LocalDateTime.now());
    }

}
