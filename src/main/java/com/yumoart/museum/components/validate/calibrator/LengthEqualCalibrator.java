package com.yumoart.museum.components.validate.calibrator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.util.StringUtils;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 长度登陆校验
 */
public class LengthEqualCalibrator implements ConstraintValidator<LengthEqual, String> {

    private int length;

    public void initialize(LengthEqual lengthEqual) {
        this.length = lengthEqual.length();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(StringUtils.hasText(value) && value.length() == length)
        {
            return true;
        }
        return false;
    }

}
