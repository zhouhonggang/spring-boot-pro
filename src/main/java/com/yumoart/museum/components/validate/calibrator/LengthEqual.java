package com.yumoart.museum.components.validate.calibrator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 长度校验器
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LengthEqualCalibrator.class)
@Repeatable(value = LengthEqual.List.class)
public @interface LengthEqual {

    int length() default 0;

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @interface List{
        LengthEqual[] value();
    }

}
