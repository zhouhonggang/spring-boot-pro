package com.yumoart.museum.components.validate.calibrator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.util.StringUtils;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-06
 * @description: 密码强度校验器实现
 */
public class PasswordCalibrator implements ConstraintValidator<Password, String> {

    /**
     * 数字规则
     */
    public static final String NUMBER = ".*\\d+.*";
    /**
     * 大写字母规则
     */
    public static final String UPPERCASE = ".*[A-Z]+.*";
    /**
     * 小写字母规则
     */
    public static final String LOWERCASE = ".*[a-z]+.*";
    /**
     * 特殊符号规则
     */
    public static final String SYMBOL = ".*[~!@#$%^&*()_+|<>,.?/:;'\\[\\]{}\"]+.*";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if(StringUtils.hasText(value) && value.length() > 6)
        {
            int count = 0;
            if (value.matches(PasswordCalibrator.SYMBOL))
                count++;
            if (value.matches(PasswordCalibrator.NUMBER))
                count++;
            if (value.matches(PasswordCalibrator.LOWERCASE))
                count++;
            if (value.matches(PasswordCalibrator.UPPERCASE))
                count++;
            return count > 2;
        }
        return false;
    }

}
