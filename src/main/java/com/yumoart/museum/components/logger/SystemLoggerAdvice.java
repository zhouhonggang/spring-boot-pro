package com.yumoart.museum.components.logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yumoart.museum.commons.annotation.LoggerOperation;
import com.yumoart.museum.projectmodules.system.service.SystemLoggerService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime 2023-01-29
 * @description: 日志通知实现
 */
@Aspect
@Component
public class SystemLoggerAdvice {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private SystemLoggerService systemLoggerService;

    /**
     * 切入点  LoggerOperation
     */
    @Pointcut("@annotation(com.yumoart.museum.commons.annotation.LoggerOperation)")
    public void method() { }

    /**
     * AOP拦截日志功能
     * @param joinPoint 连接点
     * @return  返回结果
     * @throws Throwable 异常
     */
    @Around("method()")
    public Object logger(ProceedingJoinPoint joinPoint) throws Throwable {
        //开始计时
        long start = System.currentTimeMillis();
        Object object = joinPoint.proceed();
        //结束计时
        long end = System.currentTimeMillis();
        //获取注解类
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        //获取操作类型
        LoggerOperation annotation = signature.getMethod().getAnnotation(LoggerOperation.class);
        //获取接口参数
        Object[] args = joinPoint.getArgs();
        String params = "无参数";
        if(!ObjectUtils.isEmpty(args))
            if(Arrays.stream(args).noneMatch(arg -> (arg instanceof MultipartFile || arg instanceof MultipartFile[])))
                params = objectMapper.writeValueAsString(args);
            else
                params = "文件上传";
        String result = "无结果";
        if(!ObjectUtils.isEmpty(object))
            result = objectMapper.writeValueAsString(object);
        systemLoggerService.insert(end-start, annotation.module(), annotation.type().getValue(), annotation.message(), params, result);
        return object;
    }

}
