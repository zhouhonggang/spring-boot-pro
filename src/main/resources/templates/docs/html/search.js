let api = [];
const apiDocListSize = 1
api.push({
    name: 'default',
    order: '1',
    list: []
})
api[0].list.push({
    alias: 'EducationalCampusController',
    order: '1',
    link: '教务中心-校区接口',
    desc: '教务中心-校区接口',
    list: []
})
api[0].list[0].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/educational/campus/tree',
    desc: '查询校区及下属部门岗位',
});
api[0].list.push({
    alias: 'SystemAccessoryController',
    order: '2',
    link: '系统管理-附件接口',
    desc: '系统管理-附件接口',
    list: []
})
api[0].list[1].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/accessory',
    desc: '附件上传',
});
api[0].list.push({
    alias: 'SystemAuthorityController',
    order: '3',
    link: '系统管理-菜单接口',
    desc: '系统管理-菜单接口',
    list: []
})
api[0].list[2].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/authority/tree',
    desc: '查询菜单及子菜单',
});
api[0].list.push({
    alias: 'SystemDictionaryController',
    order: '4',
    link: '系统管理-字典接口',
    desc: '系统管理-字典接口',
    list: []
})
api[0].list[3].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/page/{page}/{size}',
    desc: '分页查询',
});
api[0].list[3].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary',
    desc: '添加字典主表',
});
api[0].list[3].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/detail',
    desc: '添加字典子表',
});
api[0].list[3].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/detail/{id}',
    desc: '根据字典主键获取字典详情',
});
api[0].list[3].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/detail/code/{code}',
    desc: '根据字典编码获取集合',
});
api[0].list[3].list.push({
    order: '6',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/{id}',
    desc: '根据字典主键删除',
});
api[0].list[3].list.push({
    order: '7',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/dictionary/detail/{id}',
    desc: '根据字典详情主键删除',
});
api[0].list.push({
    alias: 'SystemRoleController',
    order: '5',
    link: '[系统管理_角色管理·api接口]',
    desc: '[系统管理_角色管理·API接口]',
    list: []
})
api[0].list[4].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role',
    desc: '添加系统管理_角色管理',
});
api[0].list[4].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role',
    desc: '编辑系统管理_角色管理',
});
api[0].list[4].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role/{id}',
    desc: '删除用户',
});
api[0].list[4].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role',
    desc: '批量删除系统管理_角色管理',
});
api[0].list[4].list.push({
    order: '5',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role/{id}',
    desc: '主键ID查询',
});
api[0].list[4].list.push({
    order: '6',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/role/page/{page}/{size}',
    desc: '条件分页查询',
});
api[0].list.push({
    alias: 'SystemUserController',
    order: '6',
    link: '系统管理-用户接口',
    desc: '系统管理-用户接口',
    list: []
})
api[0].list[5].list.push({
    order: '1',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/user',
    desc: '注册用户',
});
api[0].list[5].list.push({
    order: '2',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/user',
    desc: '编辑用户',
});
api[0].list[5].list.push({
    order: '3',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/user/{id}',
    desc: '根据主键ID查询用户属性',
});
api[0].list[5].list.push({
    order: '4',
    deprecated: 'false',
    url: 'http://127.0.0.1:9527/system/user/page/{page}/{size}',
    desc: '分页查询',
});
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code === 13) {
        const search = document.getElementById('search');
        const searchValue = search.value.toLocaleLowerCase();

        let searchGroup = [];
        for (let i = 0; i < api.length; i++) {

            let apiGroup = api[i];

            let searchArr = [];
            for (let i = 0; i < apiGroup.list.length; i++) {
                let apiData = apiGroup.list[i];
                const desc = apiData.desc;
                if (desc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                    searchArr.push({
                        order: apiData.order,
                        desc: apiData.desc,
                        link: apiData.link,
                        list: apiData.list
                    });
                } else {
                    let methodList = apiData.list || [];
                    let methodListTemp = [];
                    for (let j = 0; j < methodList.length; j++) {
                        const methodData = methodList[j];
                        const methodDesc = methodData.desc;
                        if (methodDesc.toLocaleLowerCase().indexOf(searchValue) > -1) {
                            methodListTemp.push(methodData);
                            break;
                        }
                    }
                    if (methodListTemp.length > 0) {
                        const data = {
                            order: apiData.order,
                            desc: apiData.desc,
                            link: apiData.link,
                            list: methodListTemp
                        };
                        searchArr.push(data);
                    }
                }
            }
            if (apiGroup.name.toLocaleLowerCase().indexOf(searchValue) > -1) {
                searchGroup.push({
                    name: apiGroup.name,
                    order: apiGroup.order,
                    list: searchArr
                });
                continue;
            }
            if (searchArr.length === 0) {
                continue;
            }
            searchGroup.push({
                name: apiGroup.name,
                order: apiGroup.order,
                list: searchArr
            });
        }
        let html;
        if (searchValue === '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchGroup,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            let $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiGroups, liClass, display) {
    let html = "";
    if (apiGroups.length > 0) {
        if (apiDocListSize === 1) {
            let apiData = apiGroups[0].list;
            let order = apiGroups[0].order;
            for (let j = 0; j < apiData.length; j++) {
                html += '<li class="'+liClass+'">';
                html += '<a class="dd" href="#_'+order+'_'+apiData[j].order+'_' + apiData[j].link + '">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
                html += '<ul class="sectlevel2" style="'+display+'">';
                let doc = apiData[j].list;
                for (let m = 0; m < doc.length; m++) {
                    let spanString;
                    if (doc[m].deprecated === 'true') {
                        spanString='<span class="line-through">';
                    } else {
                        spanString='<span>';
                    }
                    html += '<li><a href="#_'+order+'_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + spanString + doc[m].desc + '<span></a> </li>';
                }
                html += '</ul>';
                html += '</li>';
            }
        } else {
            for (let i = 0; i < apiGroups.length; i++) {
                let apiGroup = apiGroups[i];
                html += '<li class="'+liClass+'">';
                html += '<a class="dd" href="#_'+apiGroup.order+'_' + apiGroup.name + '">' + apiGroup.order + '.&nbsp;' + apiGroup.name + '</a>';
                html += '<ul class="sectlevel1">';

                let apiData = apiGroup.list;
                for (let j = 0; j < apiData.length; j++) {
                    html += '<li class="'+liClass+'">';
                    html += '<a class="dd" href="#_'+apiGroup.order+'_'+ apiData[j].order + '_'+ apiData[j].link + '">' +apiGroup.order+'.'+ apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
                    html += '<ul class="sectlevel2" style="'+display+'">';
                    let doc = apiData[j].list;
                    for (let m = 0; m < doc.length; m++) {
                       let spanString;
                       if (doc[m].deprecated === 'true') {
                           spanString='<span class="line-through">';
                       } else {
                           spanString='<span>';
                       }
                       html += '<li><a href="#_'+apiGroup.order+'_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">'+apiGroup.order+'.' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + spanString + doc[m].desc + '<span></a> </li>';
                   }
                    html += '</ul>';
                    html += '</li>';
                }

                html += '</ul>';
                html += '</li>';
            }
        }
    }
    return html;
}