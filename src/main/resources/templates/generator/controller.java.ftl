/*
* Copyright (c) [2022] [zhouhonggang]
*
* [spring-boot-pro] is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
*  http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*/
package ${package.Controller};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.yumoart.museum.commons.annotation.LoggerOperation;
import com.yumoart.museum.commons.enumerate.TypeEnumerate;
import com.yumoart.museum.commons.result.Result;
import com.yumoart.museum.components.validate.group.UpdateValid;
import com.yumoart.museum.projectmodules.system.entity.SystemRole;
import com.yumoart.museum.projectmodules.system.service.SystemRoleService;

import jakarta.validation.groups.Default;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *[${table.comment!}·API接口]
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-pro
 * @datetime ${date}
 * @description: [${table.comment!}·接口]
 */
@RestController
@RequestMapping("${controllerMappingHyphen?replace('-', '/')}")
public class ${table.controllerName} {

    @Autowired
    private ${table.serviceImplName} ${table.serviceImplName?uncap_first};

    /**
     * 添加${table.comment!}
     * @param entity ${table.comment!}属性
     * @return 结果集
     */
    @PostMapping
    @LoggerOperation(module = "${table.comment!}", message = "添加${table.comment!}", type = TypeEnumerate.INSERT)
    public Result insert(@RequestBody @Validated ${entity} entity) {
        return ${table.serviceImplName?uncap_first}.insert(entity);
    }

    /**
     * 编辑${table.comment!}
     * @param entity ${table.comment!}属性
     * @return 结果集
     */
    @PutMapping
    @LoggerOperation(module = "${table.comment!}", message = "编辑${table.comment!}", type = TypeEnumerate.UPDATE)
    public Result update(@RequestBody @Validated({UpdateValid.class, Default.class}) ${entity} entity) {
        return ${table.serviceImplName?uncap_first}.update(entity);
    }

    /**
     * 删除用户
     * @param id 主键id
     * @return 结果集
     */
    @DeleteMapping("{id}")
    @LoggerOperation(module = "${table.comment!}", message = "单删${table.comment!}", type = TypeEnumerate.DELETE)
    public Result delete(@PathVariable int id) {
        return ${table.serviceImplName?uncap_first}.delete(id);
    }

    /**
     * 批量删除${table.comment!}
     * @param ids 主键ids
     * @return 结果集
     */
    @DeleteMapping
    @LoggerOperation(module = "${table.comment!}", message = "批删${table.comment!}", type = TypeEnumerate.DELETE)
    public Result deletes(@RequestBody List<Integer> ids) {
        return ${table.serviceImplName?uncap_first}.deletes(ids);
    }

    /**
     * 主键ID查询
     * @param id ${table.comment!}主键ID
     * @return ${table.comment!}属性
     */
    @GetMapping("{id}")
    @LoggerOperation(module = "${table.comment!}", message = "主键id查询")
    public ${entity} load(@PathVariable int id) {
        return ${table.serviceImplName?uncap_first}.queryById(id);
    }

    /**
     * 条件分页查询
     * @param entity 分页条件
     * @return 分页属性
     */
    @GetMapping("page/{page}/{size}")
    @LoggerOperation(module = "${table.comment!}", message = "条件分页查询")
    public Page<${entity}> page(
        @PathVariable int page,
        @PathVariable int size,
        @RequestBody ${entity} entity) {
        return ${table.serviceImplName?uncap_first}.page(page, size, entity);
    }

}