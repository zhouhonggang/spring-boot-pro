/*
* Copyright (c) [2022] [zhouhonggang]
*
* [spring-boot-pro] is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
*  http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
*/
package ${package.Service};

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.yumoart.museum.commons.constants.Constants;
import ${superServiceImplClassPackage};
import com.yumoart.museum.commons.result.Result;
import ${package.Mapper}.${table.mapperName};
import ${package.Entity}.${entity};
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
* @author zhouhonggang
* @version 1.0.0
* @project spring-boot-pro
* @datetime ${date}
* @description: [${table.comment!}·逻辑]
*/
@Service
@CacheConfig(cacheNames = "${controllerMappingHyphen?replace('-', ':')}")
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}>{

    /**
     * ${table.comment!}添加
     * @param entity ${table.comment!}属性
     * @return 添加结果
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result insert(${entity} entity) {
        /** 验证添加最终结果 */
        if(dao.insert(entity) == 0)
            return Result.failure(Constants.OPERATION_ADD_FAILURE);
        /** 返回注册成功结果 */
        return Result.success(Constants.OPERATION_ADD_SUCCESS);
    }

    /**
     * ${table.comment!}编辑
     * @param entity ${table.comment!}属性
     * @return 编辑结果
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result update(${entity} entity) {
        /** 验证编辑最终结果 */
        if(dao.updateById(entity) == 0)
            return Result.failure(Constants.OPERATION_EDIT_FAILURE);
        /** 返回编辑成功结果 */
        return Result.success(Constants.OPERATION_EDIT_SUCCESS);
    }

    /**
     * ${table.comment!}删除
     * @param id 主键id
     * @return 删除结果
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result delete(int id) {
        /** 验证删除最终结果 */
        if(dao.deleteById(id) == 0)
            return Result.failure(Constants.OPERATION_DELETE_FAILURE);
        /** 返回删除成功结果 */
        return Result.success(Constants.OPERATION_DELETE_SUCCESS);
    }

    /**
     * ${table.comment!}批量删除
     * @param ids 主键id
     * @return 删除结果
     */
    @Transactional
    @CacheEvict(allEntries = true)
    public Result deletes(List<Integer> ids) {
        /** 验证删除最终结果 */
        if(dao.deleteBatchIds(ids) == 0)
            return Result.failure(Constants.OPERATION_DELETE_FAILURE);
        /** 返回删除成功结果 */
        return Result.success(Constants.OPERATION_DELETE_SUCCESS);
    }

    /**
     * 主键查询${table.comment!}
     * @param id 用户主键
     * @return 用户属性
     */
    @Cacheable(sync = true)
    public ${entity} queryById(Serializable id) {
        return dao.selectById(id);
    }

    /**
     * 按条件分页查询
     * @param entity 【${table.comment!}】+【分页】属性
     * @return 分页属性
     */
    @Cacheable(key = "'page'+#page+':'+#size+':'+#entity")
    public Page<${entity}> page(int page, int size, ${entity} entity) {
        return dao.selectPage(Page.of(page, size), new QueryWrapper<${entity}>());
    }

}
