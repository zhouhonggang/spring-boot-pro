<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC
        "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">
<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="${cacheClassName}"/>
</#if>

<#if baseColumnList>
    <!-- 通用查询结果列 -->
    <sql id="all_column">
        <#list table.commonFields as field>
            ${field.columnName},
        </#list>
        ${table.fieldNames}
    </sql>
</#if>

<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="${entity}ResultMap" type="${entity}" autoMapping="true">
    </resultMap>
</#if>

    <!--
    <select id="queryById" resultType="${entity}">
        select <include refid="all_column"/> from ${table.name} where id = <#noparse>#{id}</#noparse> and delete_flag = 0
    </select>

    <select id="queryByPage" resultType="${entity}">
        select <include refid="all_column"/> from ${table.name} where delete_flag = 0
    </select>

    <update id="delete">
        update ${table.name} set delete_flag = 1 where delete_flag = 0 and id = <#noparse>#{id}</#noparse>
    </update>

    <update id="deletes">
        update ${table.name} set delete_flag = 1 where delete_flag = 0 and id in
            <foreach collection="list" item="id" open="(" close=")" separator=",">
                <#noparse>#{id}</#noparse>
            </foreach>
    </update>
    -->
</mapper>
