package com.yumoart.museum;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootProApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("===================================================");
        System.out.println("=================Spring Boot3 Test=================");
        System.out.println("===================================================");
    }

}
