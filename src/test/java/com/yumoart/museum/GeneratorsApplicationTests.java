package com.yumoart.museum;

import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import com.yumoart.museum.projectmodules.system.service.SystemGeneratorService;
import com.yumoart.museum.projectmodules.system.service.SystemUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * 项目CRUD基础测试
 */
@SpringBootTest
class GeneratorsApplicationTests {

    @Autowired
    private SystemGeneratorService systemGeneratorService;

    // 当前表名称
    private final static String NAME = "system_role";
    // 父级模块名称
    private final static String PARENT = "system";
    // 封装模块数据
    private final Map<String, String> params = Map.of(
            "name", NAME,
            "parent", PARENT);

    // @Test
    void contextLoads() {
        systemGeneratorService.generator(params);
    }

}
