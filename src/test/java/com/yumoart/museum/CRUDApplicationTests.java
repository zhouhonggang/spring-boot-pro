package com.yumoart.museum;

import com.yumoart.museum.projectmodules.system.entity.SystemUser;
import com.yumoart.museum.projectmodules.system.service.SystemUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 项目CRUD基础测试
 */
@SpringBootTest
class CRUDApplicationTests {

    @Autowired
    private SystemUserService systemUserService;

    // @Test
    void contextLoads() {
        List<SystemUser> systemUserList = systemUserService.queryByCondition(1);
        systemUserList.forEach(System.out::println);
    }

}
